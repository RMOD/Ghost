# Ghost

Original implementation of Ghost, as described by the paper from Peck et al.
["Efficient Proxies in Smalltalk, Proceedings of ESUG International Workshop on Smalltalk Technologies (IWST'11)"](http://rmod.inria.fr/archives/papers/Mart14z-Ghost-Final.pdf)

## Installing (Tested in Pharo 7)

```Smalltalk
Metacello new 
	baseline: 'Ghost';
	repository: 'gitlab://gitlab.inria.fr:RMOD/Ghost';
	load
```

## Citing it

```
@article{Mart14z,
	Abstract = {A proxy object is a surrogate or placeholder that controls access to another target object. Proxy objects are a widely used solution for different scenarios such as remote method invocation, future objects, behavioral reflection, object databases, inter-languages communications and bindings, access control, lazy or parallel evaluation, security, among others. Most proxy implementations support proxies for regular objects but are unable to create proxies for objects with an important role in the runtime infrastructure such as classes or methods. Proxies can be complex to install, they can have a significant overhead, they can be limited to certain kind of classes, etc. Moreover, proxy implementations are often not stratified and they do not have a clear separation between proxies (the objects intercepting messages) and handlers (the objects handling interceptions). In this paper, we present Ghost: a uniform and general-purpose proxy implementation for the Pharo programming language. Ghost provides low memory consuming proxies for regular objects as well as for classes and methods.
When a proxy takes the place of a class, it intercepts both the messages received by the class and the lookup of methods for messages received by its instances. Similarly, if a proxy takes the place of a method, then the method execution is intercepted too.},
	Aeres = {ACL},
	Annote = {internationaljournal},
	Author = {Mariano Martinez Peck and Noury Bouraqadi and Luc Fabresse and Marcus Denker and Camille Teruel},
	Hal-Id = {hal-01081236},
	Journal = {Journal of Object Technology},
	Pages = {339-359},
	Volume = {98},
	Issue = {3},
	Doi = {10.1016/j.scico.2014.05.015},
	Selectif = {oui},
	Title = {Ghost: A Uniform and General-Purpose Proxy Implementation},
	Url = {http://rmod.inria.fr/archives/papers/Mart14z-Ghost-Final.pdf},
	X-Language = {EN},
    Year = {2015}}
```