Class {
	#name : #BaselineOfGhost,
	#superclass : #BaselineOf,
	#category : #BaselineOfGhost
}

{ #category : #baselines }
BaselineOfGhost >> baseline: spec [
	<baseline>
	spec
		for: #common
		do: [ spec blessing: #baseline.
			spec repository: 'gitlab://gitlab.inria.fr:RMOD/Ghost'.
			spec description: 'Original implementation of Ghost'.
			spec package: 'GhostProxies'.
			spec package: 'GhostProxiesTests' with:[spec requires: 'GhostProxies' ]]
]
